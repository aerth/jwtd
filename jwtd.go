package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"  // for jwt
	"gitlab.com/aerth/x/database"      // for saving user:hash
	"gitlab.com/aerth/x/hash/argon2id" // for hashing password
	// simple web log
)

// TODO:
//
// add salt to pw db
// revisit private key

type System struct {
	db     *database.DB
	server *http.Server
	hasher func(b []byte) []byte
	key    []byte
	Config *Config
}

type Config struct {
	Debug, CheckIP, CheckUA bool
}

type TokenFormat struct {
	Username  string
	Time      time.Time // issued
	Expires   time.Time // if set, overrides DefaultExpired per token
	UserAgent string
	IP        string
	UserInfo  UserInfo
	jwt.StandardClaims
}

type UserInfo struct {
	Permissions Permissions
}

var (
	boottime   = time.Now().UTC()
	version    string
	hits       int64
	dateformat = `Mon Jan 2 15:04:05 MST 2006`
)

var (
	DefaultExpired  = time.Minute * 10
	ErrNoToken      = errors.New("no token")
	ErrTokenExpired = errors.New("expired token, yo")
)

func main() {
	log.SetFlags(log.Ltime | log.Lshortfile)
	var (
		addr      = flag.String("addr", "127.0.0.1:8080", "address:port for listen")
		dbpath    = flag.String("db", "tokens.db", "path to BoltDB database")
		keypath   = flag.String("key", "server.key", "path to private key bytes, or - for stdin (use with gpg)")
		hashalgo  = flag.String("algo", "argon2id", "hashing algorithm to use (only argon2id supported)")
		debugflag = flag.Bool("v", false, "verbose")
		checkIP   = flag.Bool("ip", true, "check ip")
		checkUA   = flag.Bool("ua", true, "check useragent")
	)

	flag.Parse()

	// open/create db
	db, err := database.Open(*dbpath)
	if err != nil {
		log.Fatalln(err)
	}

	var keybytes []byte
	// read key (once)
	if *keypath != "-" {
		keybytes, err = ioutil.ReadFile(*keypath)
		if err != nil {
			log.Fatalln(err)
		}

	} else {
		buf := new(bytes.Buffer)
		io.Copy(buf, os.Stdin)
		keybytes = buf.Bytes()
		if len(keybytes) == 0 {
			log.Fatalln("could not read key from stdin")
		}
	}
	// create server
	sys := &System{
		db:  db,
		key: keybytes,
		server: &http.Server{
			Addr: *addr,
		},
		Config: &Config{
			Debug:   *debugflag,
			CheckIP: *checkIP,
			CheckUA: *checkUA,
		},
	}

	// set web handler
	sys.server.Handler = sys

	// set password hash
	if *hashalgo == "argon2id" {
		sys.hasher = argon2id.NewDefault().Sum
	}

	if args := flag.Args(); len(args) > 0 {
		log.Println(args)
		switch args[0] {
		case "help":
			flag.Usage()
			return
		case "addperm":
			if len(args) != 3 {
				log.Println("need 3 args, got", len(args))
				return
			}
			if err := sys.addperm(args[1], args[2]); err != nil {
				log.Fatalln(err)
			}
			log.Println("added permission")
		case "removeperm":
			if len(args) != 3 {
				log.Println("need 3 args, got", len(args))
				return
			}
			if err := sys.removeperm(args[1], args[2]); err != nil {
				log.Fatalln(err)
			}
			log.Println("removed permission")

		default:
			log.Fatalln("unknown command:", args[0])
		}
		return
	}

	// print version and listener
	go func() {
		time.Sleep(time.Second)
		fmt.Println(version)
		fmt.Printf("Serving: http://%s\n", sys.server.Addr)
	}()
	// serve http
	if err := sys.server.ListenAndServe(); err != nil {
		log.Fatalln(err)
	}
}

func NewPermissions(perm ...string) Permissions {
	m := make(Permissions)
	for _, v := range perm {
		m[v] = true
	}
	return m
}

func (s *System) validateToken(token *jwt.Token, claims *TokenFormat) bool {
	var (
		expires time.Time
	)
	if claims.Expires.IsZero() {
		expires = claims.Time.Add(DefaultExpired)
	} else {
		expires = claims.Expires
	}
	if s.Config.Debug {
		log.Printf("parsed token, user='%s' since='%s'", claims.Username, claims.Time)
	}
	// check expired
	if time.Since(expires) > 0 {
		log.Printf("expired token, user='%s' expiretime='%s'", claims.Username, expires)
		return false
	}
	signer, err := token.SigningString()
	if err != nil {
		log.Println(err)
		return false
	}
	log.Println("signed by:", signer)
	// display page
	return true
}

// used for some func
func (s *System) getkey(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}
	return s.key, nil
}

// ParseTokenForm from a form["token"], parse the form before calling
func (s *System) ParseTokenForm(r *http.Request) (*jwt.Token, *TokenFormat, error) {
	// fetch token
	tokenString := r.FormValue("token")
	if tokenString == "" {
		return nil, nil, ErrNoToken
	}
	return s.ParseTokenString(tokenString)
}

// ParseTokenString from a string
func (s *System) ParseTokenString(tokenString string) (*jwt.Token, *TokenFormat, error) {
	token, err := s.tokenparser(tokenString)
	if err != nil {
		return token, nil, err
	}

	claims, ok := token.Claims.(*TokenFormat)
	if !ok || !token.Valid {
		return token, nil, errors.New("invalid token claims")
	}
	return token, claims, nil
}

// actual parsing
func (s *System) tokenparser(tokenString string) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(tokenString, &TokenFormat{}, s.getkey)
	return token, err
}
