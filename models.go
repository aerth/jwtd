package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"log"
)

func (s *System) addperm(u, perm string) error {
	dbhash := s.db.ReadNested([]byte("users"), []byte(u), []byte("password"))
	if len(dbhash) == 0 {
		return errors.New("user is disabled or does not exist")
	}
	userinfo, err := s.readUserInfo(u)
	if err != nil {
		return err
	}
	if userinfo.Permissions == nil {
		userinfo.Permissions = make(Permissions)
	}
	userinfo.Permissions[perm] = true
	if err := s.updateUserInfo(u, userinfo); err != nil {
		return err
	}
	return nil
}
func (s *System) removeperm(u, perm string) error {
	dbhash := s.db.ReadNested([]byte("users"), []byte(u), []byte("password"))
	if len(dbhash) == 0 {
		return errors.New("user is disabled or does not exist")
	}
	userinfo, err := s.readUserInfo(u)
	if err != nil {
		return err
	}
	if userinfo.Permissions == nil {
		userinfo.Permissions = make(Permissions)
	}
	userinfo.Permissions[perm] = false
	if err := s.updateUserInfo(u, userinfo); err != nil {
		return err
	}
	return nil
}
func (s *System) checkuserpass(u, p string) bool {
	dbhash := s.db.ReadNested([]byte("users"), []byte(u), []byte("password"))
	if len(dbhash) == 0 {
		log.Println("user is disabled or does not exist")
		return false
	}

	hashed := s.hasher([]byte(p))
	if bytes.Compare(hashed, dbhash) == 0 {
		return true
	}
	//log.Printf("user provided incorrect passphrase:\n%x\n%x\n", hashed, dbhash)
	return false
}

func (s *System) checkuserExists(u string) bool {
	dbhash := s.db.ReadNested([]byte("users"), []byte(u), []byte("password"))
	return len(dbhash) != 0
}

func (s *System) updateUser(u, p string) error {
	p2 := s.hasher([]byte(p))
	return s.db.WriteNested([]byte("users"), []byte(u), []byte("password"), []byte(p2))
}
func (s *System) updateUserInfo(u string, userinfo UserInfo) error {
	encoded, err := json.Marshal(userinfo)
	if err != nil {
		return err
	}
	return s.db.WriteNested([]byte("users"), []byte(u), []byte("userinfo"), encoded)
}

func (s *System) readUserInfo(u string) (UserInfo, error) {
	var decoded = UserInfo{}
	b := s.db.ReadNested([]byte("users"), []byte(u), []byte("userinfo"))
	err := json.Unmarshal(b, &decoded)
	if err != nil {
		return UserInfo{}, nil
	}
	return decoded, nil
}
